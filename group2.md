Exercise:

Explain the following commands with one sentence:

    $ git checkout master
    $ git branch feature1
    $ git checkout -b work
    $ git checkout work
    $ git branch
    $ git branch -v
    $ git branch -a
    $ git branch --merged
    $ git branch --no-merged
    $ git branch -d work
    $ git branch -D work

Example:

    $ git status         Shows the current state of the repository

First, add your answers in this file and commit/push.
Later, integrate your answers into the README.md.