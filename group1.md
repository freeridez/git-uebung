Exercise:

Explain the following commands with one sentence:

    $ git init
    $ git clone https://...
    $ git add test.txt
    $ git reset HEAD test.txt
    $ git add .
    $ git add -u
    $ git add -A
    $ git diff --staged
    $ git commit
    $ git commit -a 
    $ git commit -m "My message"

Example:

    $ git status         Shows the current state of the repository

First, add your answers in this file and commit/push.
Later, integrate your answers into the README.md.